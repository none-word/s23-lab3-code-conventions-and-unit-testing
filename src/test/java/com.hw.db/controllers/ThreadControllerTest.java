package com.hw.db.controllers;

import com.hw.db.DAO.ThreadDAO;
import com.hw.db.DAO.UserDAO;
import com.hw.db.models.Post;
import com.hw.db.models.Thread;
import com.hw.db.models.User;
import com.hw.db.models.Vote;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.springframework.dao.DataRetrievalFailureException;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.sql.Timestamp;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ThreadControllerTest {
    private ThreadController threadController;
    private ThreadDAO threadDAO;
    private UserDAO userDAO;
    private Integer id;
    private String slug;
    private Thread thread;
    private List<Post> postsList;
    private User user;
    private Integer limit;
    private Integer since;
    private String sort;
    private Boolean desc;
    private Vote vote;

    @BeforeEach
    void setUp() {
        threadController = new ThreadController();
        id = 0;
        slug = "slug";
        thread = new Thread("Me", new Timestamp(1), "forum", "message", slug, "title", 1);
        thread.setId(id);
        postsList = List.of(new Post("Me", new Timestamp(1), "forum", "message", 0, 0, false));
        user = new User("nickname", "email", "fullname", "about");
        limit = 1;
        since = 0;
        sort = "sort";
        desc = true;
        vote = new Vote(user.getNickname(), 1);
    }

    @DisplayName("test checkIdOrSlug")
    @Nested
    class CheckIdOrSlugTest {
        @DisplayName("test id")
        @Test
        void checkIdOrSlugTest01() {
            try (MockedStatic<ThreadDAO> threadDAO = Mockito.mockStatic(ThreadDAO.class)) {
                threadDAO.when(() -> ThreadDAO.getThreadById(id)).thenReturn(thread);
                threadDAO.when(() -> ThreadDAO.getThreadBySlug(slug)).thenReturn(null);
                Thread resultThread = threadController.CheckIdOrSlug(id.toString());
                assertEquals(thread, resultThread);
            }
        }

        @DisplayName("test slug")
        @Test
        void checkIdOrSlugTest02() {
            try (MockedStatic<ThreadDAO> threadDAO = Mockito.mockStatic(ThreadDAO.class)) {
                threadDAO.when(() -> ThreadDAO.getThreadById(id)).thenReturn(null);
                threadDAO.when(() -> ThreadDAO.getThreadBySlug(slug)).thenReturn(thread);
                Thread resultThread = threadController.CheckIdOrSlug(slug);
                assertEquals(thread, resultThread);
            }
        }
    }

    @DisplayName("test createPost")
    @Nested
    class CreatePostTest {
        @DisplayName("test createPost")
        @Test
        void createPostTest01() {
            try (MockedStatic<UserDAO> userDAO = Mockito.mockStatic(UserDAO.class)) {
                try (MockedStatic<ThreadDAO> threadDAO = Mockito.mockStatic(ThreadDAO.class)) {
                    userDAO.when(() -> UserDAO.Info(postsList.get(0).getAuthor())).thenReturn(user);
                    threadDAO.when(() -> ThreadDAO.getThreadBySlug(slug)).thenReturn(thread);
                    ResponseEntity responseEntity = threadController.createPost(slug, postsList);
                    assertEquals(HttpStatus.CREATED, responseEntity.getStatusCode());
                    assertEquals(postsList, responseEntity.getBody());
                }
            }
        }

        @DisplayName("test DataAccessException after info")
        @Test
        void createPostTest02() {
            try (MockedStatic<UserDAO> userDAO = Mockito.mockStatic(UserDAO.class)) {
                try (MockedStatic<ThreadDAO> threadDAO = Mockito.mockStatic(ThreadDAO.class)) {
                    userDAO.when(() -> UserDAO.Info(postsList.get(0).getAuthor())).thenThrow(DataRetrievalFailureException.class);
                    threadDAO.when(() -> ThreadDAO.getThreadBySlug(slug)).thenReturn(thread);
                    ResponseEntity responseEntity = threadController.createPost(slug, postsList);
                    assertEquals(HttpStatus.NOT_FOUND, responseEntity.getStatusCode());
                }
            }
        }

        @DisplayName("test DataAccessException after createPosts")
        @Test
        void createPostTest03() {
            try (MockedStatic<UserDAO> userDAO = Mockito.mockStatic(UserDAO.class)) {
                try (MockedStatic<ThreadDAO> threadDAO = Mockito.mockStatic(ThreadDAO.class)) {
                    userDAO.when(() -> UserDAO.Info(postsList.get(0).getAuthor())).thenReturn(user);
                    threadDAO.when(() -> ThreadDAO.getThreadBySlug(slug)).thenReturn(thread);
                    threadDAO.when(() -> ThreadDAO.createPosts(thread, postsList, List.of(user))).thenThrow(DataRetrievalFailureException.class);
                    ResponseEntity responseEntity = threadController.createPost(slug, postsList);
                    assertEquals(HttpStatus.CONFLICT, responseEntity.getStatusCode());
                }
            }
        }
    }

    @DisplayName("test posts")
    @Nested
    class PostsTest {
        @DisplayName("test posts")
        @Test
        void postsTest01() {
            try (MockedStatic<ThreadDAO> threadDAO = Mockito.mockStatic(ThreadDAO.class)) {
                threadDAO.when(() -> ThreadDAO.getThreadBySlug(slug)).thenReturn(thread);
                threadDAO.when(() -> ThreadDAO.getPosts(id, limit, since, sort, desc)).thenReturn(postsList);
                ResponseEntity responseEntity = threadController.Posts(slug, limit, since, sort, desc);
                assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
                assertEquals(postsList, responseEntity.getBody());
            }
        }

        @DisplayName("test DataAccessException")
        @Test
        void postsTest02() {
            try (MockedStatic<ThreadDAO> threadDAO = Mockito.mockStatic(ThreadDAO.class)) {
                threadDAO.when(() -> ThreadDAO.getThreadBySlug(slug)).thenThrow(DataRetrievalFailureException.class);
                ResponseEntity responseEntity = threadController.Posts(slug, limit, since, sort, desc);
                assertEquals(HttpStatus.NOT_FOUND, responseEntity.getStatusCode());
            }
        }
    }

    @DisplayName("test change")
    @Nested
    class ChangeTest {
        @DisplayName("test change")
        @Test
        void changeTest01() {
            try (MockedStatic<ThreadDAO> threadDAO = Mockito.mockStatic(ThreadDAO.class)) {
                threadDAO.when(() -> ThreadDAO.getThreadBySlug(slug)).thenReturn(thread);
                threadDAO.when(() -> ThreadDAO.getThreadById(id)).thenReturn(thread);
                ResponseEntity responseEntity = threadController.change(slug, thread);
                assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
                assertEquals(thread, responseEntity.getBody());
            }
        }

        @DisplayName("test DataAccessException")
        @Test
        void changeTest02() {
            try (MockedStatic<ThreadDAO> threadDAO = Mockito.mockStatic(ThreadDAO.class)) {
                threadDAO.when(() -> ThreadDAO.getThreadBySlug(slug)).thenThrow(DataRetrievalFailureException.class);
                ResponseEntity responseEntity = threadController.change(slug, thread);
                assertEquals(HttpStatus.NOT_FOUND, responseEntity.getStatusCode());
            }
        }
    }

    @DisplayName("test info")
    @Nested
    class InfoTest {
        @DisplayName("test info")
        @Test
        void infoTest01() {
            try (MockedStatic<ThreadDAO> threadDAO = Mockito.mockStatic(ThreadDAO.class)) {
                threadDAO.when(() -> ThreadDAO.getThreadBySlug(slug)).thenReturn(thread);
                ResponseEntity responseEntity = threadController.info(slug);
                assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
                assertEquals(thread, responseEntity.getBody());
            }
        }

        @DisplayName("test DataAccessException")
        @Test
        void infoTest02() {
            try (MockedStatic<ThreadDAO> threadDAO = Mockito.mockStatic(ThreadDAO.class)) {
                threadDAO.when(() -> ThreadDAO.getThreadBySlug(slug)).thenThrow(DataRetrievalFailureException.class);
                ResponseEntity responseEntity = threadController.info(slug);
                assertEquals(HttpStatus.NOT_FOUND, responseEntity.getStatusCode());
            }
        }
    }

    @DisplayName("test createVote")
    @Nested
    class CreateVoteTest {
        @DisplayName("test createVote")
        @Test
        void createVoteTest01() {
            try (MockedStatic<UserDAO> userDAO = Mockito.mockStatic(UserDAO.class)) {
                try (MockedStatic<ThreadDAO> threadDAO = Mockito.mockStatic(ThreadDAO.class)) {
                    userDAO.when(() -> UserDAO.Info(user.getNickname())).thenReturn(user);
                    threadDAO.when(() -> ThreadDAO.getThreadBySlug(slug)).thenReturn(thread);
                    ResponseEntity responseEntity = threadController.createVote(slug, vote);
                    assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
                    assertEquals(thread, responseEntity.getBody());
                }
            }
        }

        @DisplayName("test DataAccessException")
        @Test
        void createVoteTest02() {
            try (MockedStatic<UserDAO> userDAO = Mockito.mockStatic(UserDAO.class)) {
                try (MockedStatic<ThreadDAO> threadDAO = Mockito.mockStatic(ThreadDAO.class)) {
                    userDAO.when(() -> UserDAO.Info(user.getNickname())).thenThrow(DataRetrievalFailureException.class);
                    threadDAO.when(() -> ThreadDAO.getThreadBySlug(slug)).thenReturn(thread);
                    ResponseEntity responseEntity = threadController.createVote(slug, vote);
                    assertEquals(HttpStatus.NOT_FOUND, responseEntity.getStatusCode());
                }
            }
        }

        @DisplayName("test DuplicateKeyException after createVote")
        @Test
        void createVoteTest03() {
            try (MockedStatic<UserDAO> userDAO = Mockito.mockStatic(UserDAO.class)) {
                try (MockedStatic<ThreadDAO> threadDAO = Mockito.mockStatic(ThreadDAO.class)) {
                    userDAO.when(() -> UserDAO.Info(user.getNickname())).thenReturn(user);
                    threadDAO.when(() -> ThreadDAO.getThreadBySlug(slug)).thenReturn(thread);
                    threadDAO.when(() -> ThreadDAO.createVote(thread, vote)).thenThrow(DuplicateKeyException.class);
                    threadDAO.when(() -> ThreadDAO.change(vote, 1)).thenReturn(1);
                    ResponseEntity responseEntity = threadController.createVote(slug, vote);
                    assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
                    assertEquals(thread, responseEntity.getBody());
                }
            }
        }

        @DisplayName("test DuplicateKeyException after change")
        @Test
        void createVoteTest04() {
            try (MockedStatic<UserDAO> userDAO = Mockito.mockStatic(UserDAO.class)) {
                try (MockedStatic<ThreadDAO> threadDAO = Mockito.mockStatic(ThreadDAO.class)) {
                    userDAO.when(() -> UserDAO.Info(user.getNickname())).thenReturn(user);
                    threadDAO.when(() -> ThreadDAO.getThreadBySlug(slug)).thenReturn(thread);
                    threadDAO.when(() -> ThreadDAO.createVote(thread, vote)).thenThrow(DuplicateKeyException.class);
                    threadDAO.when(() -> ThreadDAO.change(vote, 1)).thenThrow(DuplicateKeyException.class);
                    ResponseEntity responseEntity = threadController.createVote(slug, vote);
                    assertEquals(HttpStatus.CONFLICT, responseEntity.getStatusCode());
                }
            }
        }
    }
}